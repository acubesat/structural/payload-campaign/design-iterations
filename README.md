<div align="center">
<p>
    <a href="https://spacedot.gr/">SpaceDot 🌌🪐</a> &bull;
    <a href="https://acubesat.spacedot.gr/">AcubeSAT 🛰️🌎</a>
</p>
</div>

## Description

A repository to host design iterations for the Payload Qualification Campaign.

## Notes

Structural subsystem CAD repositories don't work with branches, because there is no need to merge code. Repositories are only used for storage of files and versioning control [naming system](https://gitlab.com/groups/acubesat/structural/3d-cads/acubesat-3d-cads/designs/-/wikis/CAD-version-control-system).